clear
echo "-------------------------------------------------"
echo "Building..."
g++ *.cpp -o exe
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "* program 1"
echo ""
echo "* EXPECTED OUTPUT: A list of variables with their values"
echo "* being displayed, as well as a list of 'max' versions for"
echo "* each pair."
echo ""
echo "* ACTUAL OUTPUT:"
echo 1 | ./exe
echo ""
echo "-------------------------------------------------"
echo "-- program1.cpp --"
cat program1.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "* program 2"
echo ""
echo "* Two vectors of items iterated through"
echo ""
echo "* ACTUAL OUTPUT:"
echo 2 | ./exe
echo ""
echo "-------------------------------------------------"
echo "-- program2.cpp --"
cat program2.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "program 3"
echo ""
echo "* A map of basic state abbreviations / state names,"
echo "* A map of student IDs to Student objects."
echo "* Ability to type in a student ID to look up,"
echo "* should handle invalid keys properly."
echo ""
echo "* ACTUAL OUTPUT:"
echo 3 1 | ./exe
echo ""
echo "* ACTUAL OUTPUT:"
echo 3 100 | ./exe
echo ""
echo "-------------------------------------------------"
echo "-- program3.cpp --"
cat program3.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "* program 4"
echo ""
echo "* Asks for an index to view a town (0-2)."
echo "* Catch out_of_range exception"
echo ""
echo "* Asks for how many sandwiches and how many people."
echo "* Catch runtime_error exception"
echo ""
echo "* ACTUAL OUTPUT - valid inputs"
echo 4 0 12 6 | ./exe
echo ""
echo "* ACTUAL OUTPUT - invalid index"
echo 4 100 12 6 | ./exe
echo ""
echo "* ACTUAL OUTPUT - division by 0"
echo 4 1 12 0 | ./exe
echo ""
echo "-------------------------------------------------"
echo "-- program4.cpp --"
cat program4.cpp
echo "-------------------------------------------------"
echo "-------------------------------------------------"
echo "* program 5"
echo ""
echo "* Displays list of courses (from vector)"
echo "* Asks for index"
echo ""
echo "* Catches out_of_range exceptions"
echo ""
echo "* ACTUAL OUTPUT - valid index"
echo 5 1 | ./exe
echo ""
echo "* ACTUAL OUTPUT - invalid index"
echo 5 100 | ./exe
echo ""
echo "-------------------------------------------------"
echo "-- program5.cpp --"
cat program5.cpp



