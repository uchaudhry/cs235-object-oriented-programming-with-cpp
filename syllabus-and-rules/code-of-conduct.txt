CODE OF CONDUCT

Since you will be interacting with other students in this course, please make sure to review this Code of Conduct: 

--------------------------------------------------
PLEDGE

We as students and instructors to make participation in our community a harassment-free experience for everyone, regardless of age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, caste, color, religion, or sexual identity and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community. 

--------------------------------------------------
STANDARDS

Examples of behavior that contributes to a positive environment for our community include:

*  Demonstrating empathy and kindness toward other people
*  Being respectful of differing opinions, viewpoints, and experiences
*  Giving and gracefully accepting constructive feedback
*  Accepting responsibility and apologizing to those affected by our mistakes, and learning from the experience
*  Focusing on what is best not just for us as individuals, but for the overall community

Examples of unacceptable behavior include:

*  The use of sexualized language or imagery, and sexual attention or advances of any kind
*  Trolling, insulting or derogatory comments, and personal or political attacks
*  Public or private harassment
*  Publishing others’ private information, such as a physical or email address, without their explicit permission
*  Other conduct which could reasonably be considered inappropriate in a professional/academic setting

--------------------------------------------------
SCOPE
This Code of Conduct applies within all course spaces, including on campus, in the discussion boards, via email, and the course Discord channel. 

--------------------------------------------------
ENFORCEMENT

Instances of abusive, harassing, or otherwise unacceptable behavior may be reported to the instructor at rsingh13@jccc.edu. All complaints will be reviewed and investigated promptly and fairly. The instructor is obligated to respect the privacy and security of the reporter of any incident. 

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

(Adapted from the Contributor Code of Conduct https://www.contributor-covenant.org/version/2/1/code_of_conduct/)
